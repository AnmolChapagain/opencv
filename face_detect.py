from flask import Flask, request, render_template, send_from_directory, redirect, url_for
import cv2
import os

app = Flask(__name__)
UPLOAD_FOLDER = 'uploads'
OUTPUT_FOLDER = 'output'
os.makedirs(UPLOAD_FOLDER, exist_ok=True)
os.makedirs(OUTPUT_FOLDER, exist_ok=True)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER

# Load the Haar cascade file
haar_cascade = cv2.CascadeClassifier('haar_face.xml')

@app.route('/')
def index():
    return render_template('index.html', filename=None)

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        return redirect(request.url)
    if file:
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
        file.save(filepath)
        
        # Process the image
        img = cv2.imread(filepath)
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_rect = haar_cascade.detectMultiScale(gray_img, scaleFactor=1.1, minNeighbors=3)
        for (x, y, w, h) in face_rect:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), thickness=2)
        
        output_filepath = os.path.join(app.config['OUTPUT_FOLDER'], file.filename)
        cv2.imwrite(output_filepath, img)
        
        return render_template('index.html', filename=file.filename)

@app.route('/output/<filename>')
def output_file(filename):
    return send_from_directory(app.config['OUTPUT_FOLDER'], filename)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port = 5000)
